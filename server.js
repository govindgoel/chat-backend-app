// src/server.js
const express = require("express");
const http = require("http");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const wsServer = require("./wsServer");

dotenv.config();
const app = express();
const server = http.createServer(app);
const markdownIt = require('markdown-it');
const md = new markdownIt();
const fs = require('fs');

const cors = require('cors');

const ALLOWED_ORIGINS = ['localhost'];
const corsOptions = {
    origin: ALLOWED_ORIGINS,
};

app.use(cors(corsOptions));

// Middleware
app.use(express.json());

// Routes
const authRoutes = require("./routes/authRoutes");
const userRoutes = require("./routes/userRoutes");
const messageRoutes = require("./routes/messageRoutes");
const groupChatRoutes = require("./routes/groupChatRoutes");
const groupMessageRoutes = require("./routes/groupMessageRoutes");

app.use("/api/auth", authRoutes);
app.use("/api/user", userRoutes);
app.use("/api/messages", messageRoutes);
app.use("/api/group/chat", groupChatRoutes);
app.use("/api/group/message", groupMessageRoutes);

app.get('/', (req, res) => {
    // Read the README.md file
    fs.readFile(__dirname + '/README.md', 'utf8', (err, data) => {
        if (err) {
            console.error('Error reading README.md:', err);
            res.status(500).send('Internal Server Error');
            return;
        }

        // Parse Markdown content into HTML
        const htmlContent = md.render(data);

        // Send HTML content as response
        res.send(htmlContent);
    });
});
// Connect to MongoDB Atlas
mongoose
  .connect(process.env.MONGODB_ATLAS_URI, {})
  .then(() => {
    console.log("Connected to MongoDB Atlas");
  })
  .catch((error) => console.error("Error connecting to MongoDB Atlas:", error));

// Initialize WebSocket server
const { io, connectedClients } = wsServer(server);

app.io = io;
app.connectedClients = connectedClients;

// if (require.main === module) {
  const PORT = process.env.PORT || 3000;
  server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
// }

module.exports = app; // Export app for use in tests
