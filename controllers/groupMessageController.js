const Message = require("../models/GroupMessage");
const GroupChat = require("../models/GroupChat");
const User = require("../models/User");

const MessageEncryptor = require("../utils/messageEncryptor");
const { uploadAttachmentToStorage } = require("./utils");

// Send Message to Group Chat Endpoint
exports.sendMessageToGroupChat = async (req, res) => {
  try {
    const { groupId, content, messageType } = req.body;
    const senderId = req.user.id;
    const sender = await User.findById(senderId);
    // Check if the group chat exists
    const groupChat = await GroupChat.findById(groupId);
    if (!groupChat) {
      return res.status(404).json({ message: "Group chat not found" });
    }

    let fileUrl = null;
    // If messageType is not "text", assume it's a file and store the file URL
    if (messageType !== "text") {
      // Assuming req.file contains the file data
      // Upload the file to Firebase Storage and get the download URL
      fileUrl = await uploadAttachmentToStorage(req.file);
    }

    // Create a new message
    const messageData = {
      sender: {
        id: senderId,
        username: sender.username,
      },
      groupChat: groupId,
      messageType,
      fileUrl,
    };

    // Encrypt the message content if it's a text message
    if (messageType === "text") {
      const messageEncryptor = new MessageEncryptor();
      const encryptedContent = messageEncryptor.encrypt(content);
      messageData.content = encryptedContent;
    } else {
      // If it's not a text message, store the content directly
      messageData.content = content;
    }

    // Save the message to the database
    const message = new Message(messageData);
    await message.save();

    // Emit WebSocket event to notify group chat participants
    req.app.io.to(groupId).emit("receive-message", message);

    res.status(201).json({ message: "Message sent successfully" });
  } catch (error) {
    console.error("Error sending message to group chat:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// Receive Messages from Group Chat
exports.receiveMessagesFromGroupChat = async (req, res) => {
  try {
    const { groupId } = req.body;
    let { page = 1, limit = 10 } = req.query;

    //Check if the user is a member of the group chat
    const isMemberOfGroup = await GroupChat.findOne({
      _id: groupId,
      members: req.user.id,
    });

    if (!isMemberOfGroup) {
      return res
        .status(403)
        .json({ message: "User is not a member of this group chat" });
    }
    
    page = parseInt(page);
    limit = parseInt(limit);

    // Calculate the number of messages to skip based on the page number and limit
    const skip = (page - 1) * limit;

    // Fetch messages for the specified group chat from the database with pagination
    const messages = await Message.find({ groupChat: groupId })
      .skip(skip)
      .limit(limit);

    // Decrypt the message content for each message
    const messageEncryptor = new MessageEncryptor();
    messages.forEach((message) => {
      if (message.messageType === "text") {
        message.content = messageEncryptor.decrypt(message.content);
      }
    });

    res.status(200).json({ messages });
  } catch (error) {
    console.error("Error receiving messages from group chat:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};
