const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../models/User");
const { admin, db } = require("../firebaseConfig");
const { uploadProfilePictureToStorage } = require("./utils");

exports.register = async (req, res) => {
  try {
    const { username, email, password, profilePicture } = req.body;

    // Check if user exists
    let user = await User.findOne({ email });
    if (user) {
      return res.status(400).json({ message: "User already exists" });
    }

    // Create new user
    user = new User({ username, email, password });
    console.log("user", user);
    // Hash password
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    console.log("profilePicture", profilePicture);
    if (!profilePicture) {
      await user.save();
      return res.status(201).json({ message: "User registered successfully" });
    }
    const profilePictureFileName = `profile-pictures/${
      user._id
    }.${new Date().toISOString()}.png`;
    const profilePictureUrl = await uploadProfilePictureToStorage(
      profilePicture,
      profilePictureFileName
    );

    user.profilePictureUrl = profilePictureUrl;
    console.log("user", user);
    await user.save();
    console.log("user", user);

    res.status(201).json({ message: "User registered successfully" });
  } catch (error) {
    console.error("Error registering user:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;

    // Check if user exists
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(400).json({ message: "Invalid credentials" });
    }

    // Check password
    const isMatch = await bcrypt.compare(password, user.password);
    console.log("isMatch", isMatch);
    if (!isMatch) {
      return res.status(400).json({ message: "Invalid credentials" });
    }

    // Generate JWT token
    const token = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, {
      expiresIn: "1h",
    });

    res.status(200).json({ token });
  } catch (error) {
    console.error("Error logging in:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};
