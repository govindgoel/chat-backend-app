// src/controllers/userController.js
const User = require("../models/User");

exports.getProfile = async (req, res) => {
    console.log(req.user, "req.user");
  try {
    const userId = req.user.id; 
    const user = await User.findById(userId).select("-password");
    if (!user) {
      res.status(404).json({ message: "User not found" });
    }
    res.status(200).json(user);
  } catch (error) {
    console.error("Error fetching user profile:", error);
    res.status(500).json({ message: "User not found" });
  }
};

exports.editProfile = async (req, res) => {
  try {
    const userId = req.user.id; // Assuming you have authentication middleware to extract user ID
    const { username, name, profilePicture } = req.body;

    const user = await User.findById(userId);
    if (!user) {
      res.status(404).json({ message: "User not found" });
    }
    if (name) user.name = name;
    if (profilePicture) user.profilePicture = profilePicture;
    if (username) user.username = username;

    await user.save();

    res.status(200).json({ message: "Profile updated successfully", user });
  } catch (error) {
    console.error("Error updating user profile:", error);
    res.status(500).json({ message: "User not found" });
  }
};
