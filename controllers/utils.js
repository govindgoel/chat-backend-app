const { admin, db } = require("../firebaseConfig");
const {
  getStorage,
  ref,
  uploadBytes,
  getDownloadURL,
} = require("firebase/storage");

const MAX_PROFILE_PICTURE_SIZE = process.env.MAX_PROFILE_PICTURE_SIZE || 2 * 1024 * 1024; // Maximum size in bytes (e.g., 5 MB)
const MAX_ATTACHMENT_SIZE = process.env.MAX_ATTACHMENT_SIZE || 10 * 1024 * 1024; // Maximum size in bytes (e.g., 10 MB)

async function uploadProfilePictureToStorage(
  profilePictureData,
  profilePictureFileName
) {
  try {
    // Check if profile picture data exceeds maximum size
    if (profilePictureData.length > MAX_PROFILE_PICTURE_SIZE) {
      throw new Error("Profile picture size exceeds the maximum allowed size.");
    }

    const storage = getStorage(admin);
    const storageRef = ref(storage, profilePictureFileName);

    // Convert base64 data to Buffer
    const buffer = Buffer.from(profilePictureData, "base64");

    // Upload image data
    await uploadBytes(storageRef, buffer);

    // Get download URL for the uploaded image
    const downloadURL = await getDownloadURL(storageRef);

    return downloadURL;
  } catch (error) {
    console.error("Error uploading profile picture:", error);
    return false;
  }
}

async function uploadAttachmentToStorage(attachmentData, attachmentFileName) {
  try {
    // Check if attachment data exceeds maximum size
    if (attachmentData.length > MAX_ATTACHMENT_SIZE) {
      throw new Error("Attachment size exceeds the maximum allowed size.");
    }

    const storage = getStorage(admin);
    const storageRef = ref(storage, attachmentFileName);

    // Convert base64 data to Buffer
    const buffer = Buffer.from(attachmentData, "base64");

    // Upload attachment data
    await uploadBytes(storageRef, buffer);

    // Get download URL for the uploaded attachment
    const downloadURL = await getDownloadURL(storageRef);

    return downloadURL;
  } catch (error) {
    console.error("Error uploading attachment:", error);
    return false;
  }
}

module.exports = { uploadProfilePictureToStorage, uploadAttachmentToStorage };
