const GroupChat = require('../models/GroupChat');

exports.createGroupChat = async (req, res) => {
  try {
    const { name, participants } = req.body;
    const groupChat = new GroupChat({ name, participants });
    await groupChat.save();
    res.status(201).json({ message: 'Group chat created successfully', groupChat });
  } catch (error) {
    console.error('Error creating group chat:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

exports.joinGroupChat = async (req, res) => {
  try {
    const { groupId } = req.body;
    const userId = req.user.id;

    const groupChat = await GroupChat.findById(groupId);
    if (!groupChat) {
      return res.status(404).json({ message: 'Group chat not found' });
    }

    groupChat.participants.push(userId);
    await groupChat.save();

    res.status(200).json({ message: 'Joined group chat successfully', groupChat });
  } catch (error) {
    console.error('Error joining group chat:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

exports.leaveGroupChat = async (req, res) => {
  try {
    const { groupId } = req.body;
    const userId = req.user.id;

    const groupChat = await GroupChat.findById(groupId);
    if (!groupChat) {
      return res.status(404).json({ message: 'Group chat not found' });
    }

    groupChat.participants.pull(userId);
    await groupChat.save();

    res.status(200).json({ message: 'Left group chat successfully' });
  } catch (error) {
    console.error('Error leaving group chat:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
};
