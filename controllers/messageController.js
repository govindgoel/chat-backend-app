const Message = require("../models/Message");
const User = require("../models/User");
const GroupChat = require("../models/GroupChat");

const { uploadAttachmentToStorage } = require("./utils");
const MessageEncryptor = require("../utils/messageEncryptor");

exports.sendMessage = async (req, res) => {
  try {
    const { receiver, content, messageType } = req.body;
    if (!receiver || !content || !messageType) {
      return res.status(400).json({ message: "Missing required fields" });
    }
    const senderId = req.user.id;

    // Fetch the sender's username
    const sender = await User.findById(senderId).select("username");

    if (!sender) {
      return res.status(404).json({ message: "Unknown sender" });
    }

    let fileUrl = null;
    const randomFileName = Math.random().toString(36).substring(7);
    // If messageType is not "text", assume it's a file and store the file URL
    if (messageType !== "text") {
      // Assuming req.file contains the file data
      // Upload the file to Firebase Storage and get the download URL
      fileUrl = await uploadAttachmentToStorage(req.file, randomFileName);
    }

    // Create a new message
    const message = new Message({
      sender: {
        id: senderId,
        username: sender.username, // Attach the sender's username
      },
      receiver,
      content,
      messageType,
      fileUrl,
    });

    // Encrypt the message content if it's a text message
    if (messageType === "text") {
      const messageEncryptor = new MessageEncryptor();
      const encryptedContent = messageEncryptor.encrypt(content);
      message.content = encryptedContent;
    }

    await message.save();

    // Emit WebSocket event to notify recipient
    req.app.io.to(receiver).emit("receive-message", message);

    res.status(201).json({ message: "Message sent successfully" });
  } catch (error) {
    console.error("Error sending message:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

exports.receiveMessage = async (req, res) => {
  try {
    const userId = req.user.id;
    //check if reciver exists
    const { page = 1, limit = 10 } = req.query; // Default page and limit values
    console.log("page", page, "limit", limit);

    // Calculate the skip value based on the page and limit
    const skip = (page - 1) * limit;

    // Fetch messages for the current user with pagination
        
    const messages = await Message.find({ receiver: userId })
      .sort({ createdAt: "desc" })
      .skip(skip)
      .limit(limit);

    // Decrypt the message content for text messages
    await Promise.all(
      messages.map(async (message) => {
        if (message.messageType === "text") {
          const messageEncryptor = new MessageEncryptor();
          try {
            message.content = messageEncryptor.decrypt(message.content);
          } catch (error) {
            console.error("Error decrypting message:", error);
            return null;
          }
        }
        return message;
      })
    );

    res.status(200).json({ messages });
  } catch (error) {
    console.error("Error receiving messages:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};
