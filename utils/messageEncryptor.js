const crypto = require("crypto");

class MessageEncryptor {
  constructor() {
    // Generate a random key and IV for encryption
    this.key = Buffer.from(process.env.ENCRYPTION_KEY, "hex");
    this.iv = Buffer.from(process.env.ENCRYPTION_IV, "hex");

    if (!this.key || !this.iv) {
      throw new Error("Encryption key and IV must be provided");
    }
  }

  encrypt(message) {
    const cipher = crypto.createCipheriv("aes-256-cbc", this.key, this.iv);
    let encryptedData = cipher.update(message, "utf-8", "base64");
    encryptedData += cipher.final("base64");
    return encryptedData;
  }

  decrypt(encryptedMessage) {
    const decipher = crypto.createDecipheriv("aes-256-cbc", this.key, this.iv);
    let decryptedData = decipher.update(encryptedMessage, "base64", "utf-8");
    decryptedData += decipher.final("utf-8");
    return decryptedData;
  }
}

module.exports = MessageEncryptor;
