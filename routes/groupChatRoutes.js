// src/routes/messageRoutes.js
const express = require("express");
const {
  joinGroupChat,
  leaveGroupChat,
  createGroupChat,
} = require("../controllers/groupChatController");
const authMiddleware = require("../middleware/authMiddleware");
const router = express.Router();

router.post("/join", authMiddleware, joinGroupChat);
router.delete("/leave", authMiddleware, leaveGroupChat);
router.post("/create", authMiddleware, createGroupChat);

module.exports = router;
