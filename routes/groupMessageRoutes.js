// src/routes/messageRoutes.js
const express = require("express");
const {
  sendMessageToGroupChat,
  receiveMessagesFromGroupChat,
} = require("../controllers/groupMessageController");
const authMiddleware = require("../middleware/authMiddleware");
const router = express.Router();

router.post("/send", authMiddleware, sendMessageToGroupChat);
router.get("/receive", authMiddleware, receiveMessagesFromGroupChat);

module.exports = router;
