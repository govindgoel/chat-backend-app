// src/routes/messageRoutes.js
const express = require('express');
const { sendMessage, receiveMessage } = require('../controllers/messageController');
const authMiddleware = require('../middleware/authMiddleware');
const router = express.Router();

router.post('/send', authMiddleware, sendMessage);
router.get('/receive', authMiddleware, receiveMessage);

module.exports = router;
