// src/routes/userRoutes.js
const express = require('express');
const { getProfile, editProfile } = require('../controllers/userController');
const authMiddleware = require('../middleware/authMiddleware');
const router = express.Router();

router.get('/profile', authMiddleware, getProfile);
router.put('/profile', authMiddleware, editProfile);

module.exports = router;
