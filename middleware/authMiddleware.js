// src/middleware/authMiddleware.js
const jwt = require("jsonwebtoken");
const User = require("../models/User");

const authMiddleware = async (req, res, next) => {
  try {
    // Get token from request header
    let token = req.header("Authorization");

    // Check if token exists
    if (!token) {
      return res
        .status(401)
        .json({ message: "Authorization denied: No token provided" });
    }

    if (token.startsWith("Bearer ")) {
      token = token.slice(7, token.length);
    }
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    // Attach user to request object
    req.user = await User.findById(decoded.userId);

    next();
  } catch (error) {
    console.error("Authentication error:", error);
    res.status(401).json({ message: "Authorization denied: Invalid token" });
  }
};

module.exports = authMiddleware;
