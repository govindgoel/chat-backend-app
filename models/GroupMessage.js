// src/models/Message.js
const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
  sender: {
    id: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    username: { type: String, required: true }
  },
  groupChat: { type: mongoose.Schema.Types.ObjectId, ref: 'GroupChat', required: true },
  fileUrl: { type: String, required: false },
  content: { type: String, required: true },
  messageType: { type: String, enum: ['text', 'image', 'video', 'attachment'], default: 'text' },
}, { timestamps: true });

module.exports = mongoose.model('GroupMessage', messageSchema);
