const request = require('supertest');
const app = require('../server'); // Assuming your Express app is exported from app.js
const User = require('../models/User');

require('dotenv').config();

// Define a test user object
const testUser = {
  username: 'testuser',
  email: 'test@example.com',
  password: 'testpassword',
};

const BASE_URL = '/api/auth';
// Describe block for authentication tests
describe('Authentication', () => {
  // Test for registering a new user
  describe('POST /register', () => {
    it('should register a new user', async () => {
      // Send a POST request to register endpoint with test user data
      const res = await request(app)
        .post(`${BASE_URL}/register`)
        .send(testUser);

      // Expect status code 201 (Created) and a success message
      expect(res.status).toBe(201);
      expect(res.body.message).toBe('User registered successfully');

      // Check if the user exists in the database
      const user = await User.findOne({ email: testUser.email });
      expect(user).toBeTruthy(); // User should exist
      expect(user.username).toBe(testUser.username); // Username should match
    });
  });

  // Test for logging in with existing user credentials
  describe('POST /login', () => {
    it('should log in with valid credentials', async () => {
      // Send a POST request to login endpoint with test user credentials
      const res = await request(app)
        .post(`${BASE_URL}/login`)
        .send({ email: testUser.email, password: testUser.password });

      // Expect status code 200 (OK) and a token in response body
      expect(res.status).toBe(200);
      expect(res.body.token).toBeTruthy(); // Token should exist
    });

    it('should return 400 for invalid credentials', async () => {
      // Send a POST request to login endpoint with invalid credentials
      const res = await request(app)
        .post('/login')
        .send({ email: 'invalid@example.com', password: 'invalidpassword' });

      // Expect status code 404
      expect(res.status).toBe(404);
    });
  });
});
