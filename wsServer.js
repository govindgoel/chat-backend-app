// wsServer.js
const socketIo = require('socket.io');

module.exports = function(server) {
  const io = socketIo(server);
  const connectedClients = new Map(); // Map to store connected clients

  io.on('connection', (socket) => {
    console.log('User connected:', socket.id);

    // Add the connected client to the map
    connectedClients.set(socket.id, socket);

    socket.on('disconnect', () => {
      console.log('User has left:', socket.id);
      // Remove the disconnected client from the map
      connectedClients.delete(socket.id);
    });

    // Handle send-message event
    socket.on('send-message', (data) => {
      console.log('Message received:', data);
      io.emit('receive-message', data);
      console.log('Message emitted:', data); // Log emitted WebSocket event
    });

  });

  io.on('error', (error) => {
    console.error('Socket.IO error:', error);
  });

  return { io, connectedClients }; // Return io and connectedClients
};
