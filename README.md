## Chat Application Backend
----
#### This is the backend for a chat application that allows users to send messages to each other and create group chats. The backend is built using Node.js, Express, and MongoDB. Real time messaging is handled using Socket.io.
---
### Features
- User authentication (register, login)
- User profile management (view, edit)
- Sending messages to users or group chats
- Receiving messages from users or group chats
- Creating group chats
- Joining and leaving group chats
- Real-time messaging using WebSockets

---
### Security
- Passwords are hashed before storing in the database.
- JSON Web Tokens (JWT) are used for user authentication.
- Routes are protected using JWT tokens.
- Cross-Origin Resource Sharing (CORS) is enabled to allow requests from the frontend.
- All models are validated using schema validation.
- All messages sent or received are encrypted using AES encryption.
- Maximum limit of file size is set for file uploads.
- Rules for file storage and database access are set to prevent unauthorized access.
---
### Production Security Considerations
- Current server uses HTTP. will be updated to HTTPS for production.
- Use a reverse proxy server to handle requests and forward them to the application server.
- Add a logging system to log requests and errors.
- Implement rate limiting to prevent abuse of the API.
---
### Installation
1. Clone the repository: `git clone
2. Install dependencies: `npm install`
3. Check `.env` file in the src directory and add the required values to environment variables:
4. Start the server: `npm start`
5. Test the endpoints using Postman or any other API testing tool.
6. Run code tests: `npm test`

---
### API Endpoints
```
- Authentication Controller
  - `POST /api/auth/register`: Registers a new user.
  - `POST /api/auth/login`: Logs in a user.

- User Controller
  - `GET /api/user/profile`: Retrieves the profile of the authenticated user.
  - `PUT /api/user/profile`: Updates the profile of the authenticated user.

- Message Controller
  - `POST /api/message/send`: Sends a message to a user or group chat.
  - `GET /api/message/receive`: Retrieves messages received by the authenticated user.

- Group Chat Controller
  - `POST /api/group/create`: Creates a new group chat.
  - `POST /api/group/join`: Adds the authenticated user to an existing group chat.
  - `POST /api/group/leave`: Removes the authenticated user from an existing group chat.

- Group Message Controller
  - `POST /api/group/message/send`: Sends a message to a group chat.
  - `GET /api/group/message/receive`: Retrieves messages received by the authenticated user in a group chat.
```


### API Documentation
## Authentication Controller

### `register`

#### Description
Registers a new user.

#### Request
- **Method:** POST
- **Endpoint:** `/api/auth/register`
- **Body Parameters:**
  - `username`: The username of the user.
  - `email`: The email address of the user.
  - `password`: The password of the user.
  - `profilePicture`: (Optional) Profile picture of the user.

#### Response
- **Status Code:** 201
- **Body:** `{ message: "User registered successfully" }`

#### Error Responses
- **Status Code:** 400
  - `{ message: "User already exists" }`
- **Status Code:** 500
  - `{ message: "Internal server error" }`

---

### `login`

#### Description
Logs in a user.

#### Request
- **Method:** POST
- **Endpoint:** `/api/auth/login`
- **Body Parameters:**
  - `email`: The email address of the user.
  - `password`: The password of the user.

#### Response
- **Status Code:** 200
- **Body:** `{ token: <JWT_TOKEN> }`

#### Error Responses
- **Status Code:** 400
  - `{ message: "Invalid credentials" }`
- **Status Code:** 500
  - `{ message: "Internal server error" }`

---
## User Controller

### `getProfile`

#### Description
Retrieves the profile of the authenticated user.

#### Request
- **Method:** GET
- **Headers:**
  - `Authorization`: Bearer token obtained from the login endpoint.
- **Endpoint:** `/api/user/profile`

#### Response
- **Status Code:** 200
- **Body:** User object excluding the password field.

#### Error Responses
- **Status Code:** 404
  - `{ message: "User not found" }`
- **Status Code:** 500
  - `{ message: "Internal server error" }`

---

### `editProfile`

#### Description
Updates the profile of the authenticated user.

#### Request
- **Method:** PUT
- **Headers:**
  - `Authorization`: Bearer token obtained from the login endpoint.
- **Endpoint:** `/api/user/profile`
- **Body Parameters:**
  - `username`: (Optional) New username of the user.
  - `name`: (Optional) New name of the user.
  - `profilePicture`: (Optional) New profile picture of the user.

#### Response
- **Status Code:** 200
- **Body:** `{ message: "Profile updated successfully", user: <updated_user_object> }`

#### Error Responses
- **Status Code:** 404
  - `{ message: "User not found" }`
- **Status Code:** 500
  - `{ message: "Internal server error" }`

---
## Message Controller

### sendMessage

Sends a message to a user or group chat.

#### Request Body
- **Method:** POST
- **Headers:**
  - `Authorization`: Bearer token obtained from the login endpoint.
- **Endpoint:** `/api/message/send`
- **Body Parameters:**
- `receiver`: ID of the recipient user or group chat.
- `content`: Content of the message.
- `messageType`: Type of the message, can be "text" or other types like "image", "video", etc.

#### Response

- `message`: Success message indicating the message was sent successfully.

#### Error Responses

- 400: Missing required fields in the request body.
- 404: Unknown sender or receiver.
- 500: Internal server error.

--- 
### receiveMessage

Retrieves messages received by the authenticated user.

#### Request Body
- **Method:** GET
- **Headers:**
  - `Authorization`: Bearer token obtained from the login endpoint.
- **Endpoint:** `/api/message/receive`
- **Query Parameters:**
- `page`: Page number for pagination.
- `limit`: Maximum number of messages per page.
#### Response

- `messages`: Array of messages received by the user.

#### Error Responses

- 500: Internal server error.

---
## Group Chat Controller

### createGroupChat

Creates a new group chat.

#### Request Body

- `name`: Name of the group chat.
- `participants`: Array of user IDs who will participate in the group chat.

#### Response

- `message`: Success message indicating the group chat was created successfully.
- `groupChat`: Details of the created group chat.

#### Error Responses

- 500: Internal server error.

### joinGroupChat

Adds the authenticated user to an existing group chat.

#### Request Body

- `groupId`: ID of the group chat to join.

#### Response

- `message`: Success message indicating the user joined the group chat successfully.
- `groupChat`: Details of the group chat after the user joined.

#### Error Responses

- 404: Group chat not found.
- 500: Internal server error.

### leaveGroupChat

Removes the authenticated user from an existing group chat.

#### Request Body

- `groupId`: ID of the group chat to leave.

#### Response

- `message`: Success message indicating the user left the group chat successfully.

#### Error Responses

- 404: Group chat not found.
- 500: Internal server error.

---

## Group Message Controller

### sendMessageToGroupChat

Sends a message to a group chat.

#### Request Body

- `groupId`: ID of the group chat.
- `content`: Content of the message.
- `messageType`: Type of the message (e.g., "text", "image", "video").

#### Response

- `message`: Success message indicating the message was sent successfully.

#### Error Responses

- 404: Group chat not found.
- 500: Internal server error.

---
### receiveMessagesFromGroupChat

Receives messages from a group chat.

#### Request Body

- `groupId`: ID of the group chat.

#### Response

- `messages`: Array of messages received from the group chat.

#### Error Responses

- 403: User is not a member of the group chat.
- 500: Internal server error.
